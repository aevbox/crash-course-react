import { useState } from 'react';
import { IProduct } from '../models/IProduct';

interface PropsProduct {
    product: IProduct;
}

export function Product({ product }: PropsProduct) {
    const [ details, setDetails ] = useState(false);
    const btnBgClassName = details ? 'bg-yellow-600' : 'bg-blue-600';
    const btnClasses = [ 'py-2 px-4 border', btnBgClassName ];
    
    return (
        <div className={ 'border py-2 px-4 rounded flex flex-col items-center mb-2' }>
            <img src={ product.image } className="w-1/6" alt={ product.title }/>
            <p>{ product.title }</p>
            <p className="font-bold">{ product.price }</p>
            <button className={ btnClasses.join(' ') }
                    onClick={ () => setDetails(prev => !prev) }
            >
                { details ? 'Show Details' : 'Hide Details' }
            </button>
            { details && <div>
                <p>{ product.description }</p>
                <span style={ { fontWeight: 600 } }>Rating: { product?.rating?.rate }</span>
            </div> }
        </div>
    );
}